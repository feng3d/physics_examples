/// <reference path="../../../feng3d/out/feng3d.d.ts" />
/// <reference path="../../../physics/out/physics.d.ts" />

if (!window.physics) window.physics = window.CANNON;

window.onload = function ()
{
    var engine = new feng3d.Engine();

    engine.scene = this.feng3d.Engine.createNewScene();

    var plane = feng3d.gameObjectFactory.createPlane();
    engine.scene.gameObject.addChild(plane);

    var sphere = feng3d.gameObjectFactory.createSphere();
    var b = sphere.getComponent(physics.Rigidbody);
    b.mass = 1;
    b.body.position.y = 5;
    engine.scene.gameObject.addChild(sphere);
};